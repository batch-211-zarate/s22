console.log('Hello World');

// Array Methods
// JS has built-in functions and methods for arrays. This allows us to manipulate and access array items

// Mutator Methods

/*

	- Mutator Methods are functions that 'mutate' or change an array after they're created
	- These methods manipulate the original array performing various tasks such as adding and removing element
*/

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
	/*
		- adds an element in the end of an array AND returns the array's length

		Syntax
			arrayName.push();
	*/

	console.log('Current Array: ');
	console.log(fruits); //log fruits array ]
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength); // 5
	console.log('Mutated array from push method: ');
	console.log(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango']

	fruits.push('Avocado', 'Guava');
	console.log('Mutated aray from push method: ');
	console.log(fruits);//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']


	// pop()
	/*
		- Removes the last elements in array AND returns the removed element

		Syntax
			arrayName.pop();
	*/

	let removedFruits = fruits.pop();
	console.log(removedFruits); //Guava
	console.log('Mutated aray from pop method: ');
	console.log(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	/*
		Mini Activity
		create a function which will unfriend the last person in the array
	*/

		let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro'];
		console.log(ghostFighters);

		function unfriend(){
			ghostFighters.pop();
		
		}

		unfriend();
		console.log(ghostFighters);

	// unshift()
	/*
		- adds one or more elements at the beginning of an array

		Syntax
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB');
	*/

	fruits.unshift('Lime', 'Banana');
	console.log('Mutated aray from unshift method: ');
	console.log(fruits); //['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	// shift()
	/*
		- removes an element at the beginning of an array AND returns the length

		Syntax
			arrayName.shift();
	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit); //Lime
	console.log('Mutated aray from shift method: ');
	console.log(fruits);//['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	// splice()
	/*
		- simultaneously removes elements from a specified index number and adds elements

		Syntax
			arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
	*/

	fruits.splice(1, 2, 'Lime', 'Cherry');
	console.log('Mutated aray from splice method: ');
	console.log(fruits);//['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	// sort()
	/*
		- rearrange the array elements in alphanumeric order

		Syntax
			arrayName.sort();
	*/

	fruits.sort();
	console.log('Mutated aray from sort method: ');
	console.log(fruits);

	// reverse()
	/*
		- reverse the order of array elements

		Syntax
			arrayName.reverse();
	*/

	fruits.reverse();
	console.log('Mutated aray from reverse method: ');
	console.log(fruits);

// Non-Mutator Methods
	/*
		- Non-Mutator methods are functions that do not modify or change an array after they are created
		- these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexOf()
	/*
		returns the index number of the first matching element found in an array
		- if no match is found, the result is -1
		- the search process will be done from the first element processing to the last element

		Syntax
			arrayName/indexOf(searchValue);
			arrayName/indexOf(searchValue, fromIndex);
	*/

	let firstIndex = countries.indexOf('PH');
	console.log('Results of indexOf method: ' + firstIndex); //1

	let invalidCountry = countries.indexOf('BR');
	console.log('Results of indexOf method: ' + invalidCountry); //-1

// lastIndexOf()
	/*
		Returns the index number of the last matching element found in an array
		-the search process will be done from the last element proceeding to the first element
		Syntax
			arrayName.lastIndexOf(searchvalue);
			arrayName.lastIndexOf(searchvalue, fromIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH', 3);
	console.log('Results of lastIndexOf method: ' + lastIndex); //1

// slice()
	/*
		- portions/slices elements from an array AND returns a new array

		Syntax
			arrayName.slice(startingIndex);
			arrayName.slice(startingIndex, endingIndex);
	*/

	let sliceArrayA = countries.slice(2);
	console.log('Results of slice method: ');
	console.log(sliceArrayA);//['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

	let sliceArrayB = countries.slice(2,4);
	console.log('Results of slice method: ');
	console.log(sliceArrayB);//['CAN', 'SG']

	let sliceArrayC = countries.slice(-3);
	console.log('Results of slice method: ');
	console.log(sliceArrayC);//['PH', 'FR', 'DE']

// toString();
	/*
		Returns an array as a string separated by commas

		Syntax
			arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log('Results of toString method: ');
	console.log(stringArray); // US,PH,CAN,SG,TH,PH,FR,DE

// concat()
	/*
		combines two arrays and returns the combined results

		Syntax
			arrayA.concat(arrayB);
			arrayA.concat(elementA);
	*/

	let taskArrayA = ['drink html', 'eat javascript'];
	let taskArrayB = ['inhale css', 'bake sass'];
	let taskArrayC = ['get git', 'be node'];

	let tasks = taskArrayA.concat(taskArrayB);
	console.log('Results of concat method: ');
	console.log(tasks); //['drink html', 'eat javascript', 'inhale css', 'bake sass']

	// Combine multiple arrays

	console.log('Results of concat method: ')
	let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
	console.log(allTasks);//['drink html', 'eat javascript', 'inhale css', 'bake sass', get git', 'be node']

	// Combining arrays with elements
	let combinedTasks = taskArrayA.concat('smell express', 'throw react');
	console.log('Results of concat method: ');
	console.log(combinedTasks);//['drink html', 'eat javascript', 'smell express', 'throw react']


// Join()
	/*
		- returns an array as a string separated by specified separator string

		Syntax
			arrayName.join('separatorString');
	*/

	let users = ['John', 'Jane', 'Joe', 'Robert', 'Nej'];
	console.log(users.join()); //John,Jane,Joe,Robert,Nej
	console.log(users.join('')); //JohnJaneJoeRobertNej
	console.log(users.join(' - ')); //John - Jane - Joe - Robert - Nej


// Iteration Method
	/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks
	*/

// forEach
	/*
		Similar to a for loop that iterates on each array element
		- for each item in the array, the anonymous function passed in the forEach() method will be run
		- the anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
		- Variable names for arrays are normally written in the plural form of the data stored in an array
		- It's common practice to use the singular form of the array content for parameter names used in array loops
		- forEach() does NOT return anything

		Syntax
			arrayName.forEach(function(indivElement){
				statement
			})
	*/

	allTasks.forEach(function(task){
		console.log(task);
	});

/*
	Mini activity #2
	create a function that can display the ghostFighters one by one in our console
*/
	function displayGhostFighters(){
		ghostFighters.forEach(function(fighters){
		console.log(fighters);
	});
	}

	displayGhostFighters();
	

// Using forEach with conditional statements

// Looping through all the array items
	/*
		It is good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

		Creating a separate variable to store results of an array iteration
		iterating methods are also good practice to avoid confusion by modifying the original array

		Mastering loops and arrays allow us developers to perform a wide range of features that help in data management and analysis
	*/

	let filteredTask = [];

	allTasks.forEach(function(task){
		if(task.length>10){
			console.log(task);
			filteredTask.push(task);
		}
	})

	console.log('Result of filtered tasks: ');
	console.log(filteredTask); // ['eat javascript']

// map()
	/*
		- iterates on each element AND returns new array with different values depending on the result of the function's operation

		Syntax
			let/const resultArray = arrayName.map(function(indivElement){
		
			})
	*/

	let numbers = [1, 2, 3, 4, 5];
	let numberMap = numbers.map(function(number){
		return number * number;
	})

	console.log('Original Array: ');
	console.log(numbers); //[1, 2, 3, 4, 5];
	console.log('Result of map method: ');
	console.log(numberMap); //[1, 4, 9, 16, 25]

	// map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	})
	console.log(numberForEach); //undefined

// every()
	/*
		- checks if all elements in an array meet the given conditions
		- this is useful for validating data stored in arrays especially when dealing with large amount of data
		- return true if all elements meet the conditions, otherwise false

		Syntax
			let/const resultArray = arrayName.every(function(indivElement){
				return expression/condition;
			})
	*/

	let allValid = numbers.every(function(number){
		return (number<3);
	});
	console.log('Result of every method: ');
	console.log(allValid); //false

// some()
	/*
		Checks if at least one element in the array meets the given condition
		-returns a true value if at least one element meets the condition and false if otherwise

		Syntax
			let/const resultArray = arrayName.some(function(indivElement){
				return expression/condition;
			})
	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	})
	console.log('Result of some method: ');
	console.log(someValid); //true

	// Combing the returned result from every/some method may be used in other statements to perfome consecutive results
	if(someValid){
		console.log('Some number in the array are greater than 2');
	}


// filter()
	/*
		returns a new array that contains elements which meets a given condition
		- return an empty array if no elements were found

		Syntax
			let/const resultArray = arrayName.filter(function(indivElement){
					return expression/condition;
				})
	*/

	let filterValid = numbers.filter(function(number){
		return (number < 3);
	});

	console.log('Result of filter method: ');
	console.log(filterValid); //[1, 2]

	let nothingFound = numbers.filter(function(number){
		return (number = 0);
	})

	console.log('Result of filter method: ');
	console.log(nothingFound); //[]

	// Filtering using forEach

	let filteredNumbers = [];
	numbers.forEach(function(number){
		// console.log(number);

		if(number<3){
			filteredNumbers.push(number);
		}
	});
	console.log('Result of filter method: ');
	console.log(filteredNumbers); //[1, 2]

// includes()
	/*
		checks if the argument passed can be found in the array
			- it returns a boolean which can be saved in a variable
				- returns true if the argument is found the array
				- returns false if it is not

			Syntax
				arrayNamme.includes(<argumentToFind>);
	*/

	let products = ['Mouse', 'Keyboards', 'Laptop', 'Monitor'];

	let productFound = products.includes('Mouse');
	console.log(productFound); //true

	let productNotFound = products.includes('Headset');
	console.log(productNotFound); //false


	// Method Chaining
		// Methods can be chained using them one after another
		// The results of the first method is used on the second method until all "chained" methods have been resolved

		let filteredProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		});

		console.log(filteredProducts); //['Keyboards', 'Laptop']

		/*
			Mini Activity 3
			create an addTrainer function that will enable us to add a trainer in the contacts array
			--This function should be able to recieve a string
			--Determine if the added trainer already exists in the contact array:
			-- if it is, show an alert saying "Already added in the Match Call"
			-- if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
			-- invoke and add a trainer in the browser's console
			-- in the console, log the contacts array
		*/

			let contact = ['Ash']

			function addTrainer(trainer) {
				/*
					if (contact.indexOf(trainer) !== -1){
						alert('Already added in the Match Call');
					}
					else{
						alert('Registered!');
						contact.push(trainer);
					}
				};
				*/

				let doesTrainerExist = contact.includes(trainer);

				if (doesTrainerExist) {
					alert('Already added in the Match Call');
				} else {
					alert('Registered!');
					contact.push(trainer);
				}

// reduce()
	/*
		- evaluates elements from left to right and returns/reduces the array into single value

		Syntax
			let/const resultArray = arrayName.reduce(function(accumalator, currentValue){
				return expression/operator
			})

			- 'accumalator' parameter in the function stores the result for every iteration of the loop
			- 'currentValue' is the current or next element in the array that is evaluated in each iteration of the loop

			How to 'reduce' method works
			1. the first/result element in the array is stored in the 'accumalator' parameter
			2. the second/next element in the array is stored in the currentvalue parameter
			3. an operation is performed ont he two elemts
			4. the loop repates step 1-3 until all elements have been worked on

	*/

	console.log(numbers);
		let iteration = 0;
		let iterationStr = 0;

		let reducedArray = numbers.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + y;
		})

		console.log("Result of reduce method: " + reducedArray);//15

		let list = ["Hello", "Again", "World"];

		let reducedJoin = list.reduce(function(x,y){
			console.warn('current iteration: ' + ++iterationStr);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + ' ' + y;
		})

		console.log("Result of reduce method: " + reducedJoin);
	}